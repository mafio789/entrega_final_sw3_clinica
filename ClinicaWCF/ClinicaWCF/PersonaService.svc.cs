﻿using ClinicaWCF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;

namespace ClinicaWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "PersonaService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione PersonaService.svc o PersonaService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class PersonaService : IPersonaService
    {
        public void ActualizarPersona(Persona persona)
        {
            try
            {
                using (var db = new ModelContainer())
                {
                    var model = db.Persona.SingleOrDefault(x => x.Id == persona.Id);

                    if (model == null)
                    {
                        throw new Exception();
                    }

                    model.Nombre = persona.Nombre;
                    model.Apellido = persona.Apellido;
                    model.Identificacion = persona.Identificacion;
                    model.FNacimiento = persona.FNacimiento;
                    model.Telefono = persona.Telefono;
                    model.Correo = persona.Correo;
                    model.Direccion = persona.Direccion;
                    model.Genero = persona.Genero;
                    model.TipoDocumento = persona.TipoDocumento;

                    if (db.ChangeTracker.HasChanges())
                    {
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public Persona ConsultarPersona(string Id)
        {
            Persona model = null;
            
            try
            {
                int personaId = Convert.ToInt32(Id);

                using (var db = new ModelContainer())
                {
                    model = db.Persona.SingleOrDefault(x => x.Id == personaId);
                }

                if (model == null)
                    throw new Exception();

                return model;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<Persona> ConsultarPersonas()
        {
            List<Persona> list = new List<Persona>();

            using (var db = new ModelContainer())
            {
                list = db.Persona.ToList();
            }

            return list;

            //try
            //{
            //    using (var db = new ModelContainer())
            //    {
            //        list = db.Persona.ToList();
            //    }

            //    return list;
            //}
            //catch (Exception)
            //{
            //    throw new WebFaultException(HttpStatusCode.BadRequest);
            //}
        }

        public void EliminarPersona(string Id)
        {
            try
            {
                int personaId = Convert.ToInt32(Id);

                using (var db = new ModelContainer())
                {
                    var model = db.Persona.SingleOrDefault(x => x.Id == personaId);

                    if (model == null)
                        throw new Exception();

                    db.Persona.Remove(model);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public void RegistrarPersona(Persona persona)
        {
            try
            {
                using (var db = new ModelContainer())
                {
                    db.Persona.Add(persona);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }
    }
}
