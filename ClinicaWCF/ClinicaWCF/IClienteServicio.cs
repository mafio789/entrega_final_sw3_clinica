﻿using ClinicaWCF.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ClinicaWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IClienteServicio" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IClienteServicio
    {
        #region OtherOperations
        [OperationContract]
        [WebInvoke(UriTemplate = "Persona", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void RegistrarPersona(Persona paciente);
        #endregion

        #region PersonaEntity
        [OperationContract]
        [WebGet(UriTemplate = "Persona/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Retorna la información especifica de PersonaEntity")]
        Persona ConsultarPersona(string id);

        [OperationContract]
        [WebGet(UriTemplate = "Personas/Odontologos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Retorna la información de todos los registros de PersonaEntity asociado a empleados que ocupan el cargo de Odontologos")]
        List<Persona> ConsultarPersonaEmpleadoOdontologo();

        [OperationContract]
        [WebInvoke(UriTemplate = "Persona", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Actualiza la información específica de un registro PersonaEntity en la base de datos")]
        void ActualizarPersona(Persona paciente);
        #endregion

        #region PacienteEntity
        [OperationContract]
        [WebInvoke(UriTemplate = "Paciente", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Retorna la información específica de PacienteEntity después de validar el usuario y contraseña de usuario")]
        Paciente AutenticarPaciente(Paciente paciente);
        #endregion

        #region AgendaEntity
        [OperationContract]
        [WebGet(UriTemplate = "Agenda/Odontologo/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Retorna la información de todos los registros de AgendaEntity asociado a empleados ocupan el cargo de Odontologos")]
        List<Agenda> ConsultarAgendaByEmpleadoOdontologo(string id);
        #endregion

        #region CitaEntity
        [OperationContract]
        [WebInvoke(UriTemplate = "Cita", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Agrega un nuevo registro de CitaEntity en la base de datos")]
        void RegistrarCita(Cita cita);

        [OperationContract]
        [WebGet(UriTemplate = "Citas/{userName}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Retorna la información de todos los registros de CitaEntity asociado a un paciente")]
        List<Cita> ConsultarCitasByPaciente(string userName);

        [OperationContract, WebInvoke(UriTemplate = "Cita/{id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Elimina la informacion específica de CitaEntity asociado a paciente en la base de datos")]
        void EliminarCita(string id);
        #endregion
    }
}
