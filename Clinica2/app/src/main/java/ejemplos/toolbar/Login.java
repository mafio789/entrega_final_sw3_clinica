package ejemplos.toolbar;

import android.content.Intent;
import android.os.AsyncTask;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Login extends Activity {

    Button login;
    EditText user;
    EditText pass;

    static String usuario;
    static int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = (Button)findViewById(R.id.login);
        user = (EditText)findViewById(R.id.user);
        pass = (EditText)findViewById(R.id.pass);

        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AutenticarLogin al = new AutenticarLogin();
                al.execute(user.getText().toString(), pass.getText().toString());
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AutenticarLogin extends AsyncTask<String,Integer,Boolean>{


        @Override
        protected Boolean doInBackground(String... params) {
            boolean resul = false;

            try {

                URL url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Paciente");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                System.out.println("user: " + params[0]);
                System.out.println("pass: " + params[1]);


                JSONObject obj = new JSONObject();
                obj.put("UserName", params[0]);
                obj.put("Password", params[1]);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream ());
                wr.writeBytes (obj.toString());
                wr.flush();
                wr.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line;

                while ((line = br.readLine()) != null)
                    sb.append(line);

                System.out.println("sb: " + sb.toString());

                JSONObject resp = new JSONObject(sb.toString());

                id = resp.getInt("Id");
                usuario = resp.getString("UserName");

                System.out.println("Usuario: " + resp.getString("UserName"));

                if(resp.getString("UserName").equals(params[0])){
                    resul = true;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            System.out.println("result:" + result);
            if(result){
                startActivity(new Intent(Login.this, main.class));

                Toast.makeText(getApplicationContext(), "Identificacion Exitosa", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), "Identificacion Fallida", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
