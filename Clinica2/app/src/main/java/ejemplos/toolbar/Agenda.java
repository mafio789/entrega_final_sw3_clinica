package ejemplos.toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import ejemplos.auxiliar.ParserFecha;
import ejemplos.auxiliar.dialogosn1;

public class Agenda extends AppCompatActivity {

    ejemplos.auxiliar.Agenda [] agendas;
    ListView agenda;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        agenda = (ListView)findViewById(R.id.agenda);

        ObtenerAgendas obtenerAgendas = new ObtenerAgendas();
        obtenerAgendas.execute();

        agenda.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogosn1 dialogo = dialogosn1.newInstance("Esta seguro de registrar esta cita?");
                dialogo.show(getSupportFragmentManager(), "dialogo");
                pos = position;
            }
        });
    }

    public void doPositiveClick(){

        RegistrarCita registrarCita = new RegistrarCita();
        registrarCita.execute("" + agendas[pos].getId());

    }

    public void doNegativeClick(){


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agenda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ObtenerAgendas extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;

            URL url;
            BufferedReader buffer;
            ParserFecha parser;

            try {
                url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Agenda/Odontologo/" + Empleados.idpersona );
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                StringBuilder fullLines = new StringBuilder();
                buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String line;

                while ((line = buffer.readLine()) != null) {
                    fullLines.append(line);
                }

                JSONArray obj = new JSONArray(fullLines.toString());
                agendas = new ejemplos.auxiliar.Agenda[obj.length()];
                parser = new ParserFecha();

                for (int i = 0; i < obj.length(); i++) {

                    ejemplos.auxiliar.Agenda agenda = new ejemplos.auxiliar.Agenda();

                    JSONObject ob = obj.getJSONObject(i);

                    agenda.setId(ob.getInt("Id"));
                    agenda.setFAgenda(parser.parser(ob.getString("FAgenda")));

                    agendas[i] = agenda;

                }

            } catch (IOException e) {
                e.printStackTrace();
                resul = false;
            } catch (Exception ex) {
                Log.e("Servicio", "Error: " + ex.getMessage());
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if(result){

                AdaptadorAgenda adaptadorAgenda = new AdaptadorAgenda(Agenda.this, agendas);
                agenda.setAdapter(adaptadorAgenda);

            }

        }

    }

    private class RegistrarCita extends AsyncTask<String,Integer,Boolean>{


        @Override
        protected Boolean doInBackground(String... params) {

            Boolean resul = true;

            try {

                URL url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Cita");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                JSONObject obj = new JSONObject();
                obj.put("Estado", "No Asignada");
                obj.put("PacienteId", Login.id);
                obj.put("AgendaId",params[0]);

                DataOutputStream wr = new DataOutputStream (
                        con.getOutputStream ());
                wr.writeBytes (obj.toString());
                wr.flush();
                wr.close();

                System.out.println("grc: " +con.getResponseCode());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                resul = false;
            } catch (IOException e) {
                e.printStackTrace();
                resul = false;
            } catch (JSONException e) {
                e.printStackTrace();
                resul = false;
            }

            return resul;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {

                Toast.makeText(getApplicationContext(), "Cita Registrada", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Agenda.this, main.class));

            }
        }
    }

    class AdaptadorAgenda extends ArrayAdapter<ejemplos.auxiliar.Agenda> {

        public AdaptadorAgenda(Context context, ejemplos.auxiliar.Agenda [] datos) {
            super(context, R.layout.list_cita, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list_cita, null);

            TextView lblTitulo = (TextView)item.findViewById(R.id.LblTitulo);
            lblTitulo.setText(agendas[position].getFAgenda());

            return(item);
        }
    }
}
