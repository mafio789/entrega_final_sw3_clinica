package ejemplos.toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import ejemplos.auxiliar.Cita;
import ejemplos.auxiliar.ParserFecha;
import ejemplos.auxiliar.dialogosn;


public class main extends AppCompatActivity {

    Cita [] citas;
    ListView cita;
    Button boton;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        cita = (ListView)findViewById(R.id.citas);
        boton = (Button)findViewById(R.id.n_cita);

        ObtenerCitas obtenerCitas = new ObtenerCitas();
        obtenerCitas.execute();

        cita.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogosn dialogo = dialogosn.newInstance("Esta seguro de cancelar esta cita?");
                dialogo.show(getSupportFragmentManager(), "dialogo");
                pos = position;
            }
        });

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toempleados = new Intent(main.this, Empleados.class);
                startActivity(toempleados);
            }
        });
    }

    public void doPositiveClick(){

        EliminarCita eliminarCita = new EliminarCita();
        eliminarCita.execute();

    }

    public void doNegativeClick(){


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ObtenerCitas extends AsyncTask<String,Integer,Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;

            URL url;
            BufferedReader buffer;
            ParserFecha parser = new ParserFecha();

            try {
                url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Citas/" + Login.usuario);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                StringBuilder fullLines = new StringBuilder();
                buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String line;

                while ((line = buffer.readLine()) != null) {
                    fullLines.append(line);
                }

                JSONArray obj = new JSONArray(fullLines.toString());
                citas = new Cita[obj.length()];

                for(int i = 0; i < obj.length(); i++){

                    Cita cita = new Cita();

                    JSONObject ob = obj.getJSONObject(i);

                    cita.setId(ob.getInt("Id"));
                    String b = ob.getString("FAsignada");
                    cita.setFreserva(parser.parser(ob.getString("FReserva")));
                    cita.setEstado(ob.getString("Estado"));



                    citas[i] = cita;
                }

            } catch (IOException e) {
                e.printStackTrace();
                resul = false;
            }catch(Exception ex) {
                Log.e("Servicio", "Error: " + ex.getMessage());
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if(result){

                AdaptadorCita adapter = new AdaptadorCita(main.this, citas);
                cita.setAdapter(adapter);

            }

        }

    }

    private class EliminarCita extends AsyncTask<String,Integer,Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;

            URL url;
            try {

                url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Cita/" + citas[pos].getId());
                HttpURLConnection con = (HttpURLConnection)url.openConnection();

                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                con.setRequestMethod("DELETE");


            } catch (MalformedURLException e) {
                e.printStackTrace();
                resul = false;
            } catch (ProtocolException e) {
                e.printStackTrace();
                resul = false;
            } catch (IOException e) {
                e.printStackTrace();
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if(result){

                Toast.makeText(getApplicationContext(), "Cita Cancelada", Toast.LENGTH_SHORT).show();

                ObtenerCitas obtenerCitas = new ObtenerCitas();
                obtenerCitas.execute();

            }

        }

    }

    class AdaptadorCita extends ArrayAdapter<Cita> {

        public AdaptadorCita(Context context, Cita [] datos) {
            super(context, R.layout.list_cita, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list_cita, null);

            TextView lblTitulo = (TextView)item.findViewById(R.id.LblTitulo);
            lblTitulo.setText("Fecha de Reserva: " + citas[position].getFreserva());

            TextView lblTitulo1 = (TextView)item.findViewById(R.id.LblTitulo2);
            lblTitulo1.setText("Fecha de Asignada: " + citas[position].getFasignada());

            TextView lblSubtitulo = (TextView)item.findViewById(R.id.LblSubTitulo);
            lblSubtitulo.setText("Estado: " + citas[position].getEstado());

            return(item);
        }
    }
}
