package ejemplos.toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ejemplos.auxiliar.Persona;


public class Empleados extends AppCompatActivity {


    ListView empleados;
    Persona [] personas;

    static int idpersona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empleados);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        empleados = (ListView)findViewById(R.id.empleados);

        ObtenerEmpleados obtenerEmpleados = new ObtenerEmpleados();
        obtenerEmpleados.execute();

        empleados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idpersona = personas[position].getId();

                Intent toagenda = new Intent(Empleados.this, Agenda.class);
                startActivity(toagenda);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empleados, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ObtenerEmpleados extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;

            URL url;
            BufferedReader buffer;

            try {
                url = new URL("http://192.168.1.10:64823/ClienteServicio.svc/Personas/Odontologos" );
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                StringBuilder fullLines = new StringBuilder();
                buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String line;

                while ((line = buffer.readLine()) != null) {
                    fullLines.append(line);
                }

                JSONArray obj = new JSONArray(fullLines.toString());

                personas = new Persona[obj.length()];

                for (int i = 0; i < obj.length(); i++) {

                    Persona persona = new Persona();

                    JSONObject ob = obj.getJSONObject(i);

                    persona.setId(ob.getInt("Id"));
                    persona.setNombre(ob.getString("Nombre"));

                    personas[i] = persona;

                }

            } catch (IOException e) {
                e.printStackTrace();
                resul = false;
            } catch (Exception ex) {
                Log.e("Servicio", "Error: " + ex.getMessage());
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if(result){

                AdaptadorPersona adaptadorPersona = new AdaptadorPersona(Empleados.this, personas);
                empleados.setAdapter(adaptadorPersona);

            }

        }

    }

    class AdaptadorPersona extends ArrayAdapter<Persona> {

        public AdaptadorPersona(Context context, Persona [] datos) {
            super(context, R.layout.list_cita, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list_cita, null);

            TextView lblTitulo = (TextView)item.findViewById(R.id.LblTitulo);
            lblTitulo.setText(personas[position].getNombre());

            return(item);
        }
    }
}
