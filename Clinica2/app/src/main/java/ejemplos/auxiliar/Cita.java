package ejemplos.auxiliar;

import java.util.Date;

public class Cita {

    private int Id;
    private String freserva;
    private String estado;
    private String fasignada;

    public void setId(int Id){
        this.Id = Id;
    }
    public int getId(){
        return Id;
    }
    public void setFreserva(String freserva){
        this.freserva = freserva;
    }
    public void setFasignada(String fasignada){
        this.fasignada = fasignada;
    }
    public void setEstado(String estado){
        this.estado = estado;
    }
    public String getFreserva(){
        return freserva;
    }
    public String getEstado(){
        return estado;
    }
    public String getFasignada(){return fasignada;}
}
