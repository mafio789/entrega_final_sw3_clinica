package ejemplos.auxiliar;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ejemplos.toolbar.main;

public class dialogosn extends DialogFragment {

    public static dialogosn newInstance(String title){
        dialogosn fragment = new dialogosn();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        String title = getArguments().getString("title");
        return new AlertDialog.Builder(getActivity())
                .setIcon(getResources().getDrawable(
                        android.R.drawable.ic_delete))
                .setTitle(title)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ((main)getActivity()).doPositiveClick();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ((main)getActivity()).doNegativeClick();
                    }
                })
                .create();
    }
}
