package ejemplos.auxiliar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ParserFecha {
    public String parser(String fecha){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        fecha = fecha.substring(0,19);
        String ackwardRipOff = fecha.replace("/Date(", "").replace(")/", "");
        Long timeInMillis = Long.valueOf(ackwardRipOff);
        calendar.setTimeInMillis(timeInMillis);
        return sdf.format(calendar.getTime());
    }
}
