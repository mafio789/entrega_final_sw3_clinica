package ejemplos.auxiliar;

public class Persona {

    private int Id;
    private String Nombre;

    public void setId(int Id){
        this.Id = Id;
    }

    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }

    public int getId(){
        return Id;
    }

    public String getNombre(){
        return Nombre;
    }
}
