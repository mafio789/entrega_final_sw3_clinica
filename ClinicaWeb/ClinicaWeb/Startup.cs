﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClinicaWeb.Startup))]
namespace ClinicaWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
