﻿using ClinicaWeb.Models.ClinicaDB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClinicaWeb.Controllers
{
    public class CitaController : Controller
    {
        private ClinicaDBContext db = new ClinicaDBContext();

        // GET: Cita
        public ActionResult Index()
        {
            List<Cita> list = db.Cita
                .Include(x=>x.Agenda)
                .Include(x => x.Agenda.Empleado.Persona)
                .Include(x => x.Paciente.Persona)
                .Where(x=>x.Estado == "A")
                .ToList();
            return View(list);
        }

        // GET: Cita/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cita/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cita/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cita/Edit/5
        public ActionResult Edit(int id)
        {
            Cita model = db.Cita
                .Include(x => x.Agenda)
                .Include(x => x.Agenda.Empleado.Persona)
                .Include(x => x.Paciente.Persona)
                .SingleOrDefault(x => x.Id == id);

            return View(model);
        }

        // POST: Cita/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, FormCollection collection)
        public ActionResult Edit(Cita cita)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var agenda = db.Agenda.Find(cita.AgendaId);
                    cita.FAsignada = new DateTime(agenda.FAgenda.Year, agenda.FAgenda.Month, agenda.FAgenda.Day, cita.FAsignada.Value.Hour, cita.FAsignada.Value.Minute, 0);
                    db.Entry(cita).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View("Edit", new { id = cita.Id });
            }
            catch
            {
                return View();
            }
        }

        // GET: Cita/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cita/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
