﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaWeb.Models.ClinicaDB;

namespace ClinicaWeb.Controllers
{
    public class PacienteController : Controller
    {
        private ClinicaDBContext db = new ClinicaDBContext();

        //// GET: Paciente
        //public ActionResult Index()
        //{
        //    var paciente = db.Paciente.Include(p => p.Persona);
        //    return View(paciente.ToList());
        //}

        //// GET: Paciente/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Paciente paciente = db.Paciente.Find(id);
        //    if (paciente == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(paciente);
        //}

        //// GET: Paciente/Create
        //public ActionResult Create()
        //{
        //    ViewBag.PersonaId = new SelectList(db.Persona, "Id", "Nombre");
        //    return View();
        //}

        // POST: Paciente/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,UserName,Password,PersonaId,FRegistro")] Paciente paciente)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Paciente.Add(paciente);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.PersonaId = new SelectList(db.Persona, "Id", "Nombre", paciente.PersonaId);
        //    return View(paciente);
        //}

        // GET: Paciente/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Paciente.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            //ViewBag.PersonaId = new SelectList(db.Persona, "Id", "Nombre", paciente.PersonaId);
            return View(paciente);
        }

        // POST: Paciente/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                Paciente model = new Paciente
                {
                    Id = paciente.Id,
                    FRegistro = paciente.FRegistro,
                    PersonaId = paciente.PersonaId,
                    UserName = paciente.UserName.ToUpper(),
                    Password = paciente.Password.ToLower()
                };

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Persona", new { id = paciente.PersonaId, type = PersonaType.Paciente });
            }

            //ViewBag.PersonaId = new SelectList(db.Persona, "Id", "Nombre", paciente.PersonaId);
            return View(paciente);
        }

        //// GET: Paciente/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Paciente paciente = db.Paciente.Find(id);
        //    if (paciente == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(paciente);
        //}

        //// POST: Paciente/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Paciente paciente = db.Paciente.Find(id);
        //    db.Paciente.Remove(paciente);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
