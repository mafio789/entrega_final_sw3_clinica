﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaWeb.Models.ClinicaDB;

namespace ClinicaWeb.Controllers
{
    public class PersonaController : Controller
    {
        private ClinicaDBContext db = new ClinicaDBContext();

        // GET: Persona
        public ActionResult Index(PersonaType? type)
        {
            List<Persona> list = new List<Persona>();
            ViewBag.PersonaType = type;

            switch (type)
            {
                case PersonaType.Paciente:
                    list = db.Paciente.Select(x => x.Persona).ToList();
                    break;
                case PersonaType.Empleado:
                    list = db.Empleado.Select(x => x.Persona).ToList();
                    break;
                default:
                    list = db.Persona.ToList();
                    break;
            }

            return View(list);
        }

        // GET: Persona/Details/5
        public ActionResult Details(int? id, PersonaType? type)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }

            switch (type)
            {
                case PersonaType.Paciente:
                    ViewBag.Type = db.Paciente.SingleOrDefault(x => x.PersonaId == persona.Id);
                    break;
                case PersonaType.Empleado:
                    ViewBag.Type = db.Empleado.SingleOrDefault(x => x.PersonaId == persona.Id);
                    break;
                default:
                    break;
            }

            ViewBag.PersonaType = type;
            return View(persona);
        }

        // GET: Persona/Create
        public ActionResult Create(PersonaType? type)
        {
            ViewBag.PersonaType = type;
            return View();
        }

        // POST: Persona/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Persona persona, PersonaType? type)
        {
            if (ModelState.IsValid)
            {
                string controllerRequest = string.Empty;
                dynamic entityRequest = null;

                Persona model = new Persona
                {
                    Nombre = persona.Nombre.ToUpper(),
                    Apellido = persona.Apellido.ToUpper(),
                    TipoDocumento = persona.TipoDocumento.ToUpper(),
                    Identificacion = persona.Identificacion,
                    FNacimiento = DateTime.Now,
                    Genero = persona.Genero.ToUpper(),
                    Telefono = persona.Telefono,
                    Correo = persona.Correo.ToLower(),
                    Direccion = persona.Direccion.ToUpper()
                };

                db.Persona.Add(model);

                switch (type)
                {
                    case PersonaType.Paciente:
                        entityRequest = db.Paciente.Add(new Paciente
                        {
                            PersonaId = model.Id,
                            UserName = string.Empty,
                            Password = string.Empty,
                            FRegistro = DateTime.Now
                        });

                        controllerRequest = "Paciente";
                        break;
                    case PersonaType.Empleado:
                        entityRequest = db.Empleado.Add(new Empleado
                        {
                            PersonaId = model.Id,
                            TipoEmpleadoId = 1,
                            FRegistro = DateTime.Now,
                            Estado = "A"
                        });

                        controllerRequest = "Empleado";
                        break;
                    default:
                        break;
                }

                db.SaveChanges();
                return RedirectToAction("Edit", controllerRequest, new { id = entityRequest.Id });
            }

            return View(persona);
        }

        // GET: Persona/Edit/5
        public ActionResult Edit(int? id, PersonaType? type)
        {
            ViewBag.PersonaType = type;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Persona/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Persona persona, PersonaType? type)
        {
            if (ModelState.IsValid)
            {
                Persona model = new Persona
                {
                    Id = persona.Id,
                    Nombre = persona.Nombre.ToUpper(),
                    Apellido = persona.Apellido.ToUpper(),
                    TipoDocumento = persona.TipoDocumento.ToUpper(),
                    Identificacion = persona.Identificacion,
                    FNacimiento = DateTime.Now,
                    Genero = persona.Genero.ToUpper(),
                    Telefono = persona.Telefono,
                    Correo = persona.Correo.ToLower(),
                    Direccion = persona.Direccion.ToUpper()
                };

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = model.Id, type = type });
            }
            return View(persona);
        }

        //// GET: Persona/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Persona persona = db.Persona.Find(id);
        //    if (persona == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(persona);
        //}

        //// POST: Persona/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Persona persona = db.Persona.Find(id);
        //    db.Persona.Remove(persona);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public enum PersonaType
    {
        Paciente,
        Empleado
    }
}
